use memmap2::MmapOptions;
use rayon::iter::ParallelIterator;

use unfolded_graph::{read_edges, Node, ZwoHashMap};

fn main() {
    let map = unsafe { MmapOptions::new().populate().map(0).unwrap() };

    // Read time-stamped edge and aggregate edge weights.
    let weights = read_edges(&map)
        .fold(
            ZwoHashMap::<(Node, Node), usize>::default,
            |mut weights, (_time, src_node, dst_node)| {
                *weights.entry((src_node, dst_node)).or_default() += 1;

                weights
            },
        )
        .reduce(
            ZwoHashMap::<(Node, Node), usize>::default,
            |mut lhs, rhs| {
                for rhs in rhs {
                    *lhs.entry(rhs.0).or_default() += rhs.1;
                }

                lhs
            },
        );

    // Collect histogram of edge weights.
    let mut values = ZwoHashMap::<usize, usize>::default();

    for weight in weights.values() {
        *values.entry(*weight).or_default() += 1;
    }

    // Make sure that they are no gaps in the histogram.
    let max_value = values.keys().copied().max().unwrap();

    for value in 1..=max_value {
        values.entry(value).or_default();
    }

    // Sort and print the histogram.
    let mut values = values.into_iter().collect::<Vec<_>>();

    values.sort_unstable_by_key(|(value, _count)| *value);

    println!("value,count");

    for (value, count) in values {
        println!("{},{}", value, count);
    }
}
