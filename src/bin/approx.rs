//! Computes the approximate path density of a time-dependent graph
//!
//! The time-dependent graph is expected in binary form passed via standard input.
//! The resolution is expected as a command line argument.
//!
//! The program will print the number of possible paths after each time step to standard output.
//! It will also print the number of nodes present in all snapshots to standard error.
//!
//! Note that in contrast to the `unfold` program which unfolds a single accessibility graph
//! using a time series of snapshots, this programs aggregates a single graph from the time series
//! and unfolds the accessibility graph of this graph after each time step.
use std::env::args;

use unfolded_graph::{read_graphs, Graph, Time, ZwoHashSet};

const USAGE: &str = "usage: approx <resolution> < <graph.bin> > <paths.csv>";

fn main() {
    let resolution = args().nth(1).expect(USAGE).parse::<Time>().expect(USAGE);

    let graphs = read_graphs(0, resolution);

    // Compute the set of nodes present in all snapshots.
    let mut nodes = ZwoHashSet::default();

    for graph in graphs.values() {
        for node in graph.nodes() {
            nodes.insert(node);
        }
    }

    eprintln!("nodes = {}", nodes.len());

    let mut aggregated_graph = Graph::default();

    println!("time,paths");

    for (time, graph) in graphs {
        // Build the aggregate graph up to this time by
        // merging all edges from the snapshot into a single graph
        // (thereby including previous snapshots).
        aggregated_graph.merge(&graph);

        // Unfold the accessibility graph of the aggregated graph from scratch.
        let unfolded_graph = aggregated_graph.unfold();

        let paths = unfolded_graph.edge_count();

        println!("{},{}", time, paths);

        // If all the possible paths are already present,
        // nothing will change in the following time steps.
        if paths == nodes.len().pow(2) {
            break;
        }
    }
}
