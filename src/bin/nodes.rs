use std::collections::BTreeMap;
use std::env::args;
use std::mem::size_of;

use memmap2::MmapOptions;
use rayon::{iter::ParallelIterator, slice::ParallelSlice};

use unfolded_graph::{Kind, Node, Time, ZwoHashSet};

const USAGE: &str = "usage: nodes <resolution> <integrate> < <graph.bin> > <nodes.csv>";

fn main() {
    let mut args = args();
    let resolution = args.nth(1).expect(USAGE).parse::<Time>().expect(USAGE);
    let integrate = args.next().expect(USAGE).parse::<bool>().expect(USAGE);

    enum NodeChange {
        Removal(Node),
        Addition(Node, Kind),
    }

    const TIME_SIZE: usize = size_of::<Time>();
    const NODE_SIZE: usize = size_of::<Node>();
    const KIND_SIZE: usize = size_of::<Kind>();
    const RECORD_SIZE: usize = TIME_SIZE + NODE_SIZE + KIND_SIZE;
    const MASK: u32 = 1 << 31 | 1 << 30;

    let map = unsafe { MmapOptions::new().populate().map(0).unwrap() };

    let changes = map
        .par_chunks_exact(RECORD_SIZE)
        .filter_map(move |chunk| {
            let (time, chunk) = chunk.split_at(TIME_SIZE);
            let time = Time::from_ne_bytes(time.try_into().unwrap());

            if time & MASK == 0 {
                return None;
            }

            assert!(time & (1 << 31) != 0);
            let removal = time & (1 << 30) == 0;

            let time = time & !MASK;

            if removal {
                let (old_node, _padding) = chunk.split_at(NODE_SIZE);
                let old_node = Node::from_ne_bytes(old_node.try_into().unwrap());

                Some((time, NodeChange::Removal(old_node)))
            } else {
                let (new_node, kind) = chunk.split_at(NODE_SIZE);
                let new_node = Node::from_ne_bytes(new_node.try_into().unwrap());
                let kind = Kind::from_ne_bytes(kind.try_into().unwrap());

                Some((time, NodeChange::Addition(new_node, kind)))
            }
        })
        .fold(
            BTreeMap::<Time, Vec<NodeChange>>::new,
            |mut changes, (time, change)| {
                changes.entry(time / resolution).or_default().push(change);

                changes
            },
        )
        .reduce(BTreeMap::<Time, Vec<NodeChange>>::new, |mut lhs, rhs| {
            for (time, mut rhs) in rhs {
                let lhs = lhs.entry(time).or_default();

                lhs.append(&mut rhs);
            }

            lhs
        });

    println!("time,nodes");

    let mut nodes = ZwoHashSet::<Node>::default();

    for (time, changes) in &changes {
        for change in changes {
            if let NodeChange::Addition(new_node, _kind) = change {
                assert!(nodes.insert(*new_node), "node {} already present", new_node);
            }
        }

        if !integrate {
            for change in changes {
                if let NodeChange::Removal(old_node) = change {
                    assert!(nodes.remove(old_node), "node {} missing", old_node);
                }
            }
        }

        println!("{},{}", time, nodes.len());
    }
}
