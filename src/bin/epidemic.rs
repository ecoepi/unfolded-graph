//! Simulates an SIS epidemic on a given time-dependent graph
//!
//! The time-dependent graph is expected in binary form passed via standard input.
//! The resolution and SIS parameters are expected as command line arguments.
//!
//! The program will print the number of infected nodes after each time step to standard output.
use std::env::args;

use rand::{
    distributions::{Bernoulli, Distribution},
    Rng, SeedableRng,
};
use rand_pcg::Pcg64;

use unfolded_graph::{read_graphs, Node, Time, ZwoHashMap, ZwoHashSet};

const USAGE: &str =
    "usage: epidemic <resolution> <transmission-probability> <recovery-time> <initial-prevalence> < <graph.bin> > <infected.csv>";

fn main() {
    let mut args = args();
    let resolution = args.nth(1).expect(USAGE).parse::<Time>().expect(USAGE);
    let transmission_probability = args.next().expect(USAGE).parse::<f64>().expect(USAGE);
    let recovery_time = args.next().expect(USAGE).parse::<Time>().expect(USAGE);
    let initial_prevalence = args.next().expect(USAGE).parse::<f64>().expect(USAGE);

    let graphs = read_graphs(0, resolution);

    let mut rng = Pcg64::from_entropy();
    let mut infected = ZwoHashSet::<Node>::default();
    let mut secondary_infections = ZwoHashMap::<Node, u32>::default();

    let transmission_probability = Bernoulli::new(transmission_probability).unwrap();
    let recovery_rate = Bernoulli::new(resolution as f64 / recovery_time as f64).unwrap();

    let first_graph = graphs.values().next().unwrap();

    for node in first_graph.nodes() {
        if rng.gen_bool(initial_prevalence) {
            infected.insert(node);
        }
    }

    println!("time,infected");

    for (time, graph) in &graphs {
        for (src_node, dst_node, count) in graph.edges() {
            if infected.contains(&src_node) && !infected.contains(&dst_node) {
                for _ in 0..count {
                    if transmission_probability.sample(&mut rng) {
                        infected.insert(dst_node);
                        *secondary_infections.entry(src_node).or_default() += 1;
                        break;
                    }
                }
            }
        }

        infected.retain(|_node| !recovery_rate.sample(&mut rng));

        println!("{},{}", time, infected.len());
    }

    let secondary_infections_mean = secondary_infections
        .values()
        .map(|value| *value as f64)
        .sum::<f64>()
        / secondary_infections.len() as f64;

    let secondary_infections_var = secondary_infections
        .values()
        .map(|value| (*value as f64 - secondary_infections_mean).powi(2))
        .sum::<f64>()
        / (secondary_infections.len() - 1) as f64;

    eprintln!(
        "secondary infections = {:.1} ± {:.1}",
        secondary_infections_mean,
        secondary_infections_var.sqrt(),
    );
}
