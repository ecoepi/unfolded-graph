//! Compares two time-dependent graphs in a way that insensitive to reorderings of edges within each snapshot
//!
//! The two time-dependent graphs are expected in binary form with their paths given as command line arguments.
//! The resolution is expected as a command line argument.
//!
//! The program has exit code 0 if the graphs are unequal and exit code 1 if they are not.
use std::env::args;
use std::fs::File;

use unfolded_graph::{read_graphs, Time};

const USAGE: &str = "usage: cmp <resolution> <lhs.bin> <rhs.bin>";

fn main() -> Result<(), &'static str> {
    let mut args = args();
    let resolution = args.nth(1).expect(USAGE).parse::<Time>().expect(USAGE);
    let lhs = args.next().expect(USAGE);
    let rhs = args.next().expect(USAGE);

    let lhs = read_graphs(&File::open(lhs).unwrap(), resolution);
    let rhs = read_graphs(&File::open(rhs).unwrap(), resolution);

    if lhs == rhs {
        Ok(())
    } else {
        Err("the given time-dependent graphs are not equal")
    }
}
