//! Computes the accessibility graph of a time-dependent graph
//!
//! The time-dependent graph is expected in binary form passed via standard input.
//! The resolution is expected as a command line argument.
//!
//! The program will print the number of possible paths and active nodes after each time step to standard output.
use std::env::args;
use std::fs::File;
use std::io::BufWriter;

use bincode::serialize_into;
use rayon::iter::{IntoParallelRefIterator, ParallelIterator};

use unfolded_graph::{dense::Graph as DenseGraph, read_graphs, Time};

const USAGE: &str = "usage: unfold <resolution> [path.bin] < <graph.bin> > <paths.csv>";

fn main() {
    let mut args = args();
    let resolution = args.nth(1).expect(USAGE).parse::<Time>().expect(USAGE);
    let path = args.next();

    let graphs = read_graphs(0, resolution);

    // Initialise an empty accessibility graph.
    let max_node = graphs
        .par_iter()
        .flat_map_iter(|(_time, graph)| graph.nodes())
        .max()
        .unwrap();

    let mut unfolded_graph = DenseGraph::new(max_node);

    println!("time,paths,nodes");

    for (time, graph) in &graphs {
        // Ensure that the zero-length paths from each node to itself is present.
        for node in graph.nodes() {
            if !unfolded_graph.contains_node(node) {
                unfolded_graph.insert(node, node);
            }
        }

        // Unfold the accessibility graph using the next snapshot.
        let paths = unfolded_graph.unfold_with(graph);
        let nodes = unfolded_graph.node_count();

        println!("{},{},{}", time, paths, nodes);
    }

    if let Some(path) = path {
        let writer = BufWriter::new(File::create(path).unwrap());
        serialize_into(writer, &unfolded_graph).unwrap();
    }
}
