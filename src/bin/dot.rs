//! Prints a snapshot in the DOT langauge using by the Graphviz package
//!
//! The time-dependent graph is expected in binary form passed via standard input.
//! The resolution and time step are expected as command line arguments.
//!
//! The program will print the DOT representation of the chosen snapshot to standard output.
use std::env::args;

use unfolded_graph::{read_graphs, Time};

const USAGE: &str = "usage: unfold <resolution> <time-step> < <graph.bin> > <snapshot.dot>";

fn main() {
    let mut args = args();
    let resolution = args.nth(1).expect(USAGE).parse::<Time>().expect(USAGE);
    let time_step = args.next().expect(USAGE).parse::<Time>().expect(USAGE);

    let graphs = read_graphs(0, resolution);
    let snapshot = graphs.get(&time_step).unwrap();

    println!("strict digraph {{");

    for (src_node, dst_node, count) in snapshot.edges() {
        println!("{} -> {} [ label=\"{}\" ];", src_node, dst_node, count);
    }

    println!("}}");
}
