//! Parses a time-dependent graph into its binary form
//!
//! Expects a time-dependent graph in textual form passed via standard input.
//! Writes its binary form to standard output.
//!
//! The textual form is one edge per line with time stamp, source node and destination node separated by white space.
//! The binary form is contiguous records of time stamp, source node and destination node in native byte order.
use std::io::{stdin, stdout, BufRead, BufReader, BufWriter, Write};

use unfolded_graph::{Node, Time};

fn main() {
    let stdin = stdin();
    let mut stdin = BufReader::with_capacity(1024 * 1024, stdin.lock());

    let stdout = stdout();
    let mut stdout = BufWriter::with_capacity(1024 * 1024, stdout.lock());

    let mut line = String::new();
    while stdin.read_line(&mut line).unwrap() != 0 {
        line.pop();

        let mut fields = line.split_ascii_whitespace();

        let time = fields.next().unwrap().parse::<Time>().unwrap();
        let src_node = fields.next().unwrap().parse::<Node>().unwrap();
        let dst_node = fields.next().unwrap().parse::<Node>().unwrap();

        line.clear();

        stdout.write_all(&time.to_ne_bytes()).unwrap();
        stdout.write_all(&src_node.to_ne_bytes()).unwrap();
        stdout.write_all(&dst_node.to_ne_bytes()).unwrap();
    }
}
