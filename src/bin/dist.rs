//! Computes the minimum path lengths of a time-dependent graph
//!
//! The time-dependent graph is expected in binary form passed via standard input.
//! The resolution is expected as a command line argument.
//!
//! The program will print the distribution of path lengths to standard output.
use std::env::args;

use unfolded_graph::{read_graphs, Graph, Time, ZwoHashMap, ZwoHashSet};

const USAGE: &str = "usage: dist <resolution> <until> < <graph.bin> > <distances.csv>";

fn main() {
    let mut args = args();
    let resolution = args.nth(1).expect(USAGE).parse::<Time>().expect(USAGE);
    let until = args.next().expect(USAGE).parse::<Time>().expect(USAGE);

    let graphs = read_graphs(0, resolution);

    // Initialise an empty accessibility graph containing only trivial paths from each node to itself.
    let mut unfolded_graph = Graph::default();

    let nodes = graphs
        .values()
        .flat_map(|graph| graph.nodes())
        .collect::<ZwoHashSet<_>>();

    for node in nodes {
        unfolded_graph.insert(node, node);
    }

    for (time, graph) in &graphs {
        if *time > until {
            break;
        }

        eprintln!("time = {time}");

        // Unfold the accessibility graph using the next snapshot.
        unfolded_graph.unfold_with(graph);
    }

    // Collect a histogram of path lengths.
    let mut values = ZwoHashMap::<u32, u32>::default();

    for (_src_node, _dst_node, distance) in unfolded_graph.edges() {
        *values.entry(distance).or_default() += 1;
    }

    // Make sure that they are no gaps in the histogram.
    let max_value = values.keys().copied().max().unwrap();

    for value in 0..=max_value {
        values.entry(value).or_default();
    }

    // Sort and print the histogram.
    let mut values = values.into_iter().collect::<Vec<_>>();

    values.sort_unstable_by_key(|(value, _count)| *value);

    println!("value,count");

    for (value, count) in values {
        println!("{},{}", value, count);
    }
}
