//! Compute the histograms of incoming and outgoing node degrees of an aggregated time-dependent graph
//!
//! The time-dependent graph is expected in binary form passed via standard input.
//! The resolution and upper time limit are expected as command line arguments.
//!
//! The program will print the histograms of incoming and outgoing node degrees to standard output.
use std::env::args;

use unfolded_graph::{read_aggregated_graph, Time, ZwoHashMap};

const USAGE: &str = "usage: deg <resolution> <until> <weighted> < <graph.bin> > <histograms.csv>";

fn main() {
    let mut args = args();
    let resolution = args.nth(1).expect(USAGE).parse::<Time>().expect(USAGE);
    let until = args.next().expect(USAGE).parse::<Time>().expect(USAGE);
    let weighted = args.next().expect(USAGE).parse::<bool>().expect(USAGE);

    // Read the aggregated graph up to the given time limit.
    let aggregated_graph = read_aggregated_graph(0, resolution, until);

    // Collect two histograms of the outgoing and incoming node degrees.
    let mut values = ZwoHashMap::<usize, (usize, usize)>::default();

    if weighted {
        for (_node, out_deg) in aggregated_graph.weighted_out_degrees() {
            values.entry(out_deg).or_default().0 += 1;
        }

        for (_node, in_deg) in aggregated_graph.weighted_in_degrees() {
            values.entry(in_deg).or_default().1 += 1;
        }
    } else {
        for (_node, out_deg) in aggregated_graph.out_degrees() {
            values.entry(out_deg).or_default().0 += 1;
        }

        for (_node, in_deg) in aggregated_graph.in_degrees() {
            values.entry(in_deg).or_default().1 += 1;
        }
    }

    // Make sure that they are no gaps in the histograms.
    let max_value = values.keys().copied().max().unwrap();

    for value in 0..=max_value {
        values.entry(value).or_default();
    }

    // Sort and print the histograms.
    let mut values = values.into_iter().collect::<Vec<_>>();

    values.sort_unstable_by_key(|(value, _counts)| *value);

    println!("value,count_out_deg,count_in_deg");

    for (value, (count_out_deg, count_in_deg)) in values {
        println!("{},{},{}", value, count_out_deg, count_in_deg);
    }
}
