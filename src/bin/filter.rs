use std::env::args;
use std::fs::File;
use std::io::{BufWriter, Write};
use std::mem::size_of;
use std::os::unix::io::FromRawFd;

use memmap2::MmapOptions;

use unfolded_graph::{Node, Time};

const USAGE: &str = "filter <from> <until> < <graph.bin> > <graph.bin>";

fn main() {
    let mut args = args();
    let from = args.nth(1).expect(USAGE).parse::<Time>().expect(USAGE);
    let until = args.next().expect(USAGE).parse::<Time>().expect(USAGE);

    let stdin = unsafe { MmapOptions::new().populate().map(0).unwrap() };
    let mut stdout = BufWriter::with_capacity(1024 * 1024, unsafe { File::from_raw_fd(1) });

    const TIME_SIZE: usize = size_of::<Time>();
    const NODE_SIZE: usize = size_of::<Node>();
    const RECORD_SIZE: usize = TIME_SIZE + 2 * NODE_SIZE;

    stdin.chunks_exact(RECORD_SIZE).for_each(|chunk| {
        let (time, _) = chunk.split_at(TIME_SIZE);
        let time = Time::from_ne_bytes(time.try_into().unwrap());

        if from <= time && time < until {
            stdout.write_all(chunk).unwrap();
        }
    });
}
