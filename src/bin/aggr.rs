//! Computes the time series of aggregated graphs of a time-dependent graph
//!
//! The time-dependent graph is expected in binary form passed via standard input.
//! The resolution is expected as a command line argument.
//!
//! The program will print the number of edges present in the aggregated graph after each time step to standard output.
use std::env::args;

use unfolded_graph::{read_graphs, Graph, Time};

const USAGE: &str = "usage: aggr <resolution> < <graph.bin> > <edges.csv>";

fn main() {
    let resolution = args().nth(1).expect(USAGE).parse::<Time>().expect(USAGE);

    let graphs = read_graphs(0, resolution);

    let mut aggregated_graph = Graph::default();
    let mut merged_edges = 0;

    println!("time,edges,merged_edges");

    for (time, graph) in graphs {
        // Build the aggregate graph up to this time by
        // merging all edges from the snapshot into a single graph
        // (thereby including previous snapshots).
        aggregated_graph.merge(&graph);

        let edges = aggregated_graph.edge_count();

        merged_edges += graph.weighted_edge_count();

        println!("{time},{edges},{merged_edges}");
    }
}
