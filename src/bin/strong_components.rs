//! Computes the sizes of the strongly connected components given an accessibility graph of a time-dependent graph
//!
//! The unfolded accessibility graph is expected in serialized form passed as a command line argument.
//!
//! The program will print the size of the specified number of random components in descending order to standard output.
use std::cmp::Reverse;
use std::env::args;
use std::fs::File;
use std::io::BufReader;

use bincode::deserialize_from;

use unfolded_graph::dense::Graph as DenseGraph;

const USAGE: &str = "usage: strong_components <samples> <path.bin> > <components.csv>";

fn main() {
    let mut args = args();
    let samples = args.nth(1).expect(USAGE).parse::<usize>().expect(USAGE);
    let path = args.next().expect(USAGE);

    let reader = BufReader::new(File::open(path).unwrap());
    let unfolded_graph: DenseGraph = deserialize_from(reader).unwrap();

    let mut components = unfolded_graph.strongly_connected_components(samples);

    components.sort_unstable_by_key(|component| Reverse(*component));

    println!("size");

    for component in components {
        println!("{}", component);
    }
}
