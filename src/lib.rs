use std::collections::BTreeMap;
use std::convert::TryInto;
use std::hash::BuildHasherDefault;
use std::mem::size_of;

use hashbrown::{
    hash_map::{Entry, HashMap},
    HashSet,
};
use memmap2::{Mmap, MmapAsRawDesc, MmapOptions};
use rayon::{
    iter::{IntoParallelRefMutIterator, ParallelIterator},
    slice::ParallelSlice,
};
use zwohash::ZwoHasher;

pub mod dense;

pub type ZwoHashMap<K, V> = HashMap<K, V, BuildHasherDefault<ZwoHasher>>;
pub type ZwoHashSet<T> = HashSet<T, BuildHasherDefault<ZwoHasher>>;

/// The type used to represent time stamps
pub type Time = u32;
/// The type used to represent node identifiers
pub type Node = u32;
/// The type used to represent node kinds
pub type Kind = u32;

/// Read time-stamped edges from `file` and aggregate them into a time series of snapshots at `resolution`.
pub fn read_graphs(file: impl MmapAsRawDesc, resolution: Time) -> BTreeMap<Time, Graph> {
    let map = unsafe { MmapOptions::new().populate().map(file).unwrap() };

    read_edges(&map)
        .fold(
            BTreeMap::<Time, Graph>::new,
            |mut graphs, (time, src_node, dst_node)| {
                let graph = graphs.entry(time / resolution).or_default();

                graph.insert(src_node, dst_node);

                graphs
            },
        )
        .reduce(BTreeMap::<Time, Graph>::new, |mut lhs, rhs| {
            for (time, rhs) in rhs {
                let lhs = lhs.entry(time).or_default();

                lhs.merge(&rhs);
            }

            lhs
        })
}

/// Read time-stamped edges from `file` and aggregate all edges whose time stamp at `resolution` is less then `until` into a single graph
pub fn read_aggregated_graph(file: impl MmapAsRawDesc, resolution: Time, until: Time) -> Graph {
    let map = unsafe { MmapOptions::new().populate().map(file).unwrap() };

    read_edges(&map)
        .fold(
            Graph::default,
            |mut aggregated_graph, (time, src_node, dst_node)| {
                if time / resolution <= until {
                    aggregated_graph.insert(src_node, dst_node);
                }

                aggregated_graph
            },
        )
        .reduce(Graph::default, |mut lhs, rhs| {
            lhs.merge(&rhs);

            lhs
        })
}

/// Read time-stamped edges from `map` at `resolution`
pub fn read_edges(map: &Mmap) -> impl ParallelIterator<Item = (Time, Node, Node)> + '_ {
    const TIME_SIZE: usize = size_of::<Time>();
    const NODE_SIZE: usize = size_of::<Node>();
    const RECORD_SIZE: usize = TIME_SIZE + 2 * NODE_SIZE;
    const MASK: u32 = 1 << 31 | 1 << 30;

    map.par_chunks_exact(RECORD_SIZE).filter_map(move |chunk| {
        let (time, chunk) = chunk.split_at(TIME_SIZE);
        let time = Time::from_ne_bytes(time.try_into().unwrap());

        if time & MASK != 0 {
            return None;
        }

        let (src_node, dst_node) = chunk.split_at(NODE_SIZE);
        let src_node = Node::from_ne_bytes(src_node.try_into().unwrap());
        let dst_node = Node::from_ne_bytes(dst_node.try_into().unwrap());

        Some((time, src_node, dst_node))
    })
}

/// The primary data structure used to represent directed graphs
///
/// For each node, `outgoing` contains the set of nodes reachable via an outgoing edge.
#[derive(Default, Clone, PartialEq)]
pub struct Graph {
    outgoing: ZwoHashMap<Node, ZwoHashMap<Node, u32>>,
}

impl Graph {
    /// Insert an edge from `src_node` to `dst_node`
    ///
    /// * Inserts `dst_node` into the set of nodes reachable from `src_node` via an outgoing edge.
    /// * Makes sure `dst_node` is present in the set of nodes, i.e. the keys of `self.outgoing`.
    pub fn insert(&mut self, src_node: Node, dst_node: Node) {
        *self
            .outgoing
            .entry(src_node)
            .or_default()
            .entry(dst_node)
            .or_default() += 1;

        self.outgoing.entry(dst_node).or_default();
    }

    /// Merge `other` into `self`
    ///
    /// All edges from `other` are added to `self`.
    pub fn merge(&mut self, other: &Self) {
        self.outgoing.par_iter_mut().for_each(|(node, outgoing)| {
            if let Some(other) = other.outgoing.get(node) {
                outgoing.extend(other);
            }
        });

        other.outgoing.iter().for_each(|(&node, other)| {
            if let Entry::Vacant(entry) = self.outgoing.entry(node) {
                entry.insert(other.clone());
            }
        });
    }

    /// Unfold the accessibility graph of a time-independent graph
    ///
    /// Computes the graph of paths of `self`.
    ///
    /// * Starts from zero-length paths connecting each node to itself.
    /// * Repeatedly extends those paths using the outgoing edges of `self`.
    /// * Stops when the number of paths has saturated.
    pub fn unfold(&self) -> Graph {
        let mut unfolded_graph = Graph::default();

        for &node in self.outgoing.keys() {
            unfolded_graph.insert(node, node);
        }

        let mut paths = unfolded_graph.edge_count();

        loop {
            unfolded_graph.unfold_with(self);

            let new_paths = unfolded_graph.edge_count();

            if paths != new_paths {
                paths = new_paths;
            } else {
                break;
            }
        }

        unfolded_graph
    }

    /// Unfold the accessibility graph of a time-dependent graph
    ///
    /// Updates the accessibility graph stored in `self` using the snapshot `other`.
    ///
    /// * Iterates in parallel over all nodes in `self`.
    /// * Iterates over all nodes already reachable via outgoing edges, i.e. reachable via paths of length less than or equal to N.
    /// * Using the edges of `other`, computes the newly reachable nodes via paths of length N+1.
    /// * Inserts those newly reachable nodes into the set of nodes reachable via outgoing edges.
    pub fn unfold_with(&mut self, other: &Self) {
        self.outgoing.par_values_mut().for_each_init(
            ZwoHashMap::<Node, u32>::default,
            |all_newly_reachable, reachable| {
                for (node, &distance) in reachable.iter() {
                    if let Some(outgoing) = other.outgoing.get(node) {
                        let new_distance = distance + 1;

                        for &new_node in outgoing.keys() {
                            let distance =
                                all_newly_reachable.entry(new_node).or_insert(new_distance);

                            if *distance > new_distance {
                                *distance = new_distance;
                            }
                        }
                    }
                }

                for (new_node, new_distance) in all_newly_reachable.drain() {
                    let distance = reachable.entry(new_node).or_insert(new_distance);

                    if *distance > new_distance {
                        *distance = new_distance;
                    }
                }
            },
        );
    }

    /// Returns an iterator over the nodes of `self`
    ///
    /// Represented by node identifiers.
    pub fn nodes(&self) -> impl Iterator<Item = Node> + '_ {
        self.outgoing.keys().copied()
    }

    /// Returns an iterator over the edges of `self`
    ///
    /// Represented as pairs of node identifiers, first source then destination.
    pub fn edges(&self) -> impl Iterator<Item = (Node, Node, u32)> + '_ {
        self.outgoing.iter().flat_map(|(&src_node, outgoing)| {
            outgoing
                .iter()
                .map(move |(&dst_node, &count)| (src_node, dst_node, count))
        })
    }

    /// The number of nodes in `self`
    pub fn node_count(&self) -> usize {
        self.outgoing.len()
    }

    /// The number of edges in `self`
    pub fn edge_count(&self) -> usize {
        self.outgoing
            .par_values()
            .map(|outgoing| outgoing.len())
            .sum()
    }

    /// The number of edges in `self` weighted by their multiplicity
    pub fn weighted_edge_count(&self) -> usize {
        self.outgoing
            .par_values()
            .map(|outgoing| {
                outgoing
                    .values()
                    .map(|&count| count as usize)
                    .sum::<usize>()
            })
            .sum()
    }

    /// Returns an iterator over the outgoing degrees of all nodes in `self`
    ///
    /// Represented as pairs of node identifier and degree and ignores edge multiplicity.
    pub fn out_degrees(&self) -> impl ExactSizeIterator<Item = (Node, usize)> + '_ {
        self.outgoing
            .iter()
            .map(|(node, outgoing)| (*node, outgoing.len()))
    }

    /// Returns an iterator over the outgoing degrees of all nodes in `self` weighted by their multiplicity
    ///
    /// Represented as pairs of node identifier and degree and considers edge multiplicity.
    pub fn weighted_out_degrees(&self) -> impl ExactSizeIterator<Item = (Node, usize)> + '_ {
        self.outgoing
            .iter()
            .map(|(node, outgoing)| (*node, outgoing.values().map(|&count| count as usize).sum()))
    }

    /// Returns an iterator the over incoming degrees of all nodes in `self`
    ///
    /// Represented as pairs of node identifier and degree and ignores edge multiplicity.
    pub fn in_degrees(&self) -> impl ExactSizeIterator<Item = (Node, usize)> + '_ {
        self.outgoing.keys().map(move |node| {
            let incoming = self
                .outgoing
                .par_values()
                .filter(|outgoing| outgoing.contains_key(node))
                .count();

            (*node, incoming)
        })
    }

    /// Returns an iterator the over incoming degrees of all nodes in `self` weighted by their multiplicity
    ///
    /// Represented as pairs of node identifier and degree and considers edge multiplicity.
    pub fn weighted_in_degrees(&self) -> impl ExactSizeIterator<Item = (Node, usize)> + '_ {
        self.outgoing.keys().map(move |node| {
            let incoming = self
                .outgoing
                .par_values()
                .filter_map(|outgoing| outgoing.get(node).map(|&count| count as usize))
                .sum();

            (*node, incoming)
        })
    }
}
