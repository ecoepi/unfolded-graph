use std::iter::from_fn;

use rand::{Rng, SeedableRng};
use rand_pcg::Pcg64Mcg;
use rayon::{
    iter::{IndexedParallelIterator, IntoParallelIterator, ParallelIterator},
    slice::ParallelSliceMut,
};
use serde::{Deserialize, Serialize};

use super::{Graph as SparseGraph, Node};

#[derive(Serialize, Deserialize)]
pub struct Graph {
    max_node: usize,
    /// Stores 1 + 1 + max_node bitmaps of size 1 + max_node.
    /// The bitmap at index zero indicates which nodes are present in the graph.
    /// The bitmaps from index one indicate which nodes are reachable via an outgoing edge.
    bits: Bits,
}

impl Graph {
    /// Creates an empty dense graph prepared to store node identifiers up to `max_node`.
    pub fn new(max_node: Node) -> Self {
        let max_node = max_node as usize;

        Self {
            max_node,
            bits: Bits::new(1 + 1 + max_node, 1 + max_node),
        }
    }

    /// Insert an edge from `src_node` to `dst_node`
    ///
    /// * Sets the bits indicating presence of `src_node` and `dst_node` in the zeroth bitmap.
    /// * Set the bit indicating an edge to `dst_node` in bitmap of edges outgoing from `src_node`.
    pub fn insert(&mut self, src_node: Node, dst_node: Node) {
        let mut bits = self.bits.as_slice_mut();

        let mut nodes = bits.get_mut(0);

        nodes.set(src_node as usize);
        nodes.set(dst_node as usize);

        let mut edges = bits.get_mut(1 + src_node as usize);

        edges.set(dst_node as usize);
    }

    pub fn contains_node(&self, node: Node) -> bool {
        let bits = self.bits.as_slice();
        let nodes = bits.get(0);

        nodes.test(node as usize)
    }

    pub fn node_count(&self) -> usize {
        let bits = self.bits.as_slice();
        let nodes = bits.get(0);

        nodes.count()
    }

    /// Unfold the accessibility graph of a time-dependent graph
    ///
    /// Updates the accessibility graph stored in `self` using the snapshot `other`.
    /// Returns the new number of edges in `self`.
    ///
    /// * Iterates in parallel over all bitmaps of outgoing edges in `self`.
    /// * Iterates over all nodes already reachable via outgoing edges, i.e. reachable via paths of length less than or equal to N.
    /// * Using the edges of `other`, marks all newly reachable nodes via paths of length N+1.
    /// * Merges those newly reachable nodes into the set of nodes reachable via outgoing edges.
    pub fn unfold_with(&mut self, other: &SparseGraph) -> usize {
        let mut bits = self.bits.as_slice_mut();

        let (mut nodes, mut edges) = bits.split_at_mut(1);
        let nodes = nodes.get_mut(0);
        let nodes = nodes.as_ref();

        edges
            .par_iter_mut()
            .enumerate()
            .map_init(
                || Bits::new(1, 1 + self.max_node),
                |buf, (src_node, mut edges)| {
                    if nodes.test(src_node) {
                        let mut buf = buf.as_slice_mut();
                        let mut buf = buf.get_mut(0);

                        buf.clear();
                        edges.as_ref().iter().for_each(|dst_node| {
                            if let Some(outgoing) = other.outgoing.get(&(dst_node as Node)) {
                                for dst_node in outgoing.keys() {
                                    buf.set(*dst_node as usize);
                                }
                            }
                        });

                        edges.merge(buf.as_ref());
                        edges.as_ref().count()
                    } else {
                        0
                    }
                },
            )
            .sum()
    }

    /// Draw samples from the set of strongly connected components
    ///
    /// As the reachability relation implied by the accessibility graph of a time-dependent graph is not necessarily transitive,
    /// it does not uniquely define connected components. However, we can still construct maximal fully connected subsets of the nodes
    /// and sample from the distribution of their size which is what is relevant epidemiologically.
    pub fn strongly_connected_components(&self, samples: usize) -> Vec<usize> {
        self.connected_components(samples, |edges, node, other_node| {
            edges.get(node).test(other_node) && edges.get(other_node).test(node)
        })
    }

    /// Draw samples from the set of weakly connected components
    ///
    /// As the reachability relation implied by the accessibility graph of a time-dependent graph is not necessarily transitive,
    /// it does not uniquely define connected components. However, we can still construct maximal fully connected subsets of the nodes
    /// and sample from the distribution of their size which is what is relevant epidemiologically.
    pub fn weakly_connected_components(&self, samples: usize) -> Vec<usize> {
        self.connected_components(samples, |edges, node, other_node| {
            edges.get(node).test(other_node) || edges.get(other_node).test(node)
        })
    }

    fn connected_components<C>(&self, samples: usize, connected: C) -> Vec<usize>
    where
        C: Fn(&BitsSlice, usize, usize) -> bool + Send + Sync,
    {
        let bits = self.bits.as_slice();
        let (nodes, edges) = bits.split_at(1);
        let nodes = nodes.get(0);

        (0..samples)
            .into_par_iter()
            .map_init(
                || (Pcg64Mcg::from_entropy(), Bits::new(2, 1 + self.max_node)),
                |(rng, bits), _| {
                    let mut bits = bits.as_slice_mut();
                    let (mut component, mut fully_connected) = bits.split_at_mut(1);
                    let mut component = component.get_mut(0);
                    let mut fully_connected = fully_connected.get_mut(0);

                    component.clear();
                    fully_connected.assign(nodes);

                    let mut count = nodes.count();

                    while count != 0 {
                        let index = rng.gen_range(0..count);

                        let node = fully_connected.as_ref().iter().nth(index).unwrap();

                        component.set(node);
                        fully_connected.reset(node);

                        count -= 1;

                        fully_connected.retain(|other_node| {
                            let connected = connected(&edges, node, other_node);

                            if !connected {
                                count -= 1;
                            }

                            connected
                        });
                    }

                    // Check that the component is fully connected and maximal.
                    assert!(component.as_ref().iter().all(|node| {
                        component
                            .as_ref()
                            .iter()
                            .all(|other_node| connected(&edges, node, other_node))
                    }));

                    fully_connected.assign(nodes);
                    fully_connected.remove(component.as_ref());

                    assert!(fully_connected.as_ref().iter().all(|node| {
                        !component
                            .as_ref()
                            .iter()
                            .all(|other_node| connected(&edges, node, other_node))
                    }));

                    component.as_ref().count()
                },
            )
            .collect()
    }
}

#[derive(Serialize, Deserialize)]
struct Bits {
    len: usize,
    val: Box<[u64]>,
}

impl Bits {
    fn new(cnt: usize, len: usize) -> Self {
        let len = (len + 63) / 64;

        Self {
            len,
            val: (0..cnt * len).map(|_| 0).collect(),
        }
    }

    fn as_slice(&self) -> BitsSlice {
        BitsSlice {
            len: self.len,
            val: &self.val,
        }
    }

    fn as_slice_mut(&mut self) -> BitsSliceMut {
        BitsSliceMut {
            len: self.len,
            val: &mut self.val,
        }
    }
}

#[derive(Clone, Copy)]
struct BitsSlice<'a> {
    len: usize,
    val: &'a [u64],
}

impl BitsSlice<'_> {
    fn get(&self, idx: usize) -> BitsRef {
        let val = &self.val[idx * self.len..][..self.len];

        BitsRef { val }
    }

    fn split_at(&self, idx: usize) -> (BitsSlice, BitsSlice) {
        let (lhs, rhs) = self.val.split_at(idx * self.len);

        let lhs = BitsSlice {
            len: self.len,
            val: lhs,
        };

        let rhs = BitsSlice {
            len: self.len,
            val: rhs,
        };

        (lhs, rhs)
    }
}

struct BitsSliceMut<'a> {
    len: usize,
    val: &'a mut [u64],
}

impl BitsSliceMut<'_> {
    fn get_mut(&mut self, idx: usize) -> BitsMut {
        let val = &mut self.val[idx * self.len..][..self.len];

        BitsMut { val }
    }

    fn split_at_mut(&mut self, idx: usize) -> (BitsSliceMut, BitsSliceMut) {
        let (lhs, rhs) = self.val.split_at_mut(idx * self.len);

        let lhs = BitsSliceMut {
            len: self.len,
            val: lhs,
        };

        let rhs = BitsSliceMut {
            len: self.len,
            val: rhs,
        };

        (lhs, rhs)
    }

    fn par_iter_mut(&mut self) -> impl IndexedParallelIterator<Item = BitsMut> + '_ {
        self.val
            .par_chunks_exact_mut(self.len)
            .map(|val| BitsMut { val })
    }
}

#[derive(Clone, Copy)]
struct BitsRef<'a> {
    val: &'a [u64],
}

impl<'a> BitsRef<'a> {
    fn test(self, bit: usize) -> bool {
        self.val[bit / 64] & (1 << (bit % 64)) != 0
    }

    fn count(self) -> usize {
        self.val.iter().map(|val| val.count_ones() as usize).sum()
    }

    fn iter(self) -> impl Iterator<Item = usize> + 'a {
        self.val.iter().enumerate().flat_map(|(idx, val)| {
            let off = 64 * idx;
            let mut val = *val;

            from_fn(move || {
                if val != 0 {
                    let bit = val.trailing_zeros();
                    val &= !(1 << bit);
                    Some(off + bit as usize)
                } else {
                    None
                }
            })
        })
    }
}

struct BitsMut<'a> {
    val: &'a mut [u64],
}

impl BitsMut<'_> {
    fn as_ref(&self) -> BitsRef {
        BitsRef { val: self.val }
    }

    fn set(&mut self, bit: usize) {
        self.val[bit / 64] |= 1 << (bit % 64);
    }

    fn reset(&mut self, bit: usize) {
        self.val[bit / 64] &= !(1 << (bit % 64));
    }

    fn clear(&mut self) {
        self.val.fill(0);
    }

    fn assign(&mut self, other: BitsRef) {
        self.val.copy_from_slice(other.val);
    }

    fn merge(&mut self, other: BitsRef) {
        debug_assert_eq!(self.val.len(), other.val.len());

        self.val
            .iter_mut()
            .zip(other.val)
            .for_each(|(val, other_val)| *val |= *other_val);
    }

    fn remove(&mut self, other: BitsRef) {
        debug_assert_eq!(self.val.len(), other.val.len());

        self.val
            .iter_mut()
            .zip(other.val)
            .for_each(|(val, other_val)| *val &= !other_val);
    }

    fn retain<F>(&mut self, mut f: F)
    where
        F: FnMut(usize) -> bool,
    {
        self.val.iter_mut().enumerate().for_each(|(idx, val)| {
            let off = 64 * idx;
            let mut tmp = *val;

            while tmp != 0 {
                let bit = tmp.trailing_zeros();
                let mask = !(1 << bit);
                tmp &= mask;

                if !f(off + bit as usize) {
                    *val &= mask;
                }
            }
        })
    }
}
